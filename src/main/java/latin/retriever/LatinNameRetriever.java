package latin.retriever;

import static latin.country.LatinNameAssembler.latinName;

import latin.country.Country;
import latin.country.Language;
import latin.country.LatinName;
import latin.country.LatinNameAssembler;
import latin.country.Name;
import latin.country.Translation;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public final class LatinNameRetriever {
    public static LatinName getLatinName(Country country) {
        var name = getPrimaryNameFor(country);
        var latinName = getLatinNameFor(country);

        return latinName().withName(name).withLatinName(latinName).build();
    }

    private static String getLatinNameFor(final Country country) {
        return prioritizedLatinNameTranslation(country)
                .orElseGet(() -> primaryNameLatinTranslation(country));
    }

    private static String getPrimaryNameFor(Country country) {
        return country
                .names()
                .stream()
                .filter(LatinNameRetriever::standardName)
                .map(Name::translations)
                .flatMap(Collection::stream)
                .filter(Translation::isPrimaryName)
                .map(Translation::name)
                .findFirst()
                .orElse("");
    }

    private static Optional<String> prioritizedLatinNameTranslation(Country country) {
        return country
                .names()
                .stream()
                .filter(LatinNameRetriever::standardName)
                .map(Name::translations)
                .flatMap(Collection::stream)
                .filter(TranslationPriority::priorityLanguage)
                .max(TranslationPriority::compare)
                .map(Translation::name);
    }

    private static String primaryNameLatinTranslation(Country country) {

        return country
                .names()
                .stream()
                .filter(LatinNameRetriever::standardName)
                .map(Name::translations)
                .flatMap(Collection::stream)
                .filter(Translation::isPrimaryName)
                .map(Translation::transliteration)
                .findFirst()
                .orElse("");
    }

    private static boolean standardName(final Name name) {
        return name.type().equals(Name.Type.Standard);
    }

    private static class TranslationPriority {
        private static final Map<Language, Integer> priorities = new HashMap<>();

        static {
            priorities.put(Language.English, 2);
            priorities.put(Language.French, 1);
        }

        static int compare(final Translation translation1, final Translation translation2) {
            var firstPriority = priorities.getOrDefault(translation1.language(), 0);
            var secondPriority = priorities.getOrDefault(translation2.language(), 0);
            return Integer.compare(firstPriority, secondPriority);
        }

        static boolean priorityLanguage(Translation translation) {

            return priorities.containsKey(translation.language());
        }
    }
}
