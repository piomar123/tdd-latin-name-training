package latin.country;

import java.util.List;

public final class Country {
    private List<Name> names;

    public Country(List<Name> names) {
        this.names = names;
    }

    public List<Name> names() {
        return names;
    }
}
