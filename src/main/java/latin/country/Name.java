package latin.country;

import java.util.List;

public final class Name {

    private List<Translation> translations;
    private final Type type;

    public Name(Type type, List<Translation> transliterations) {

        this.type = type;
        translations = transliterations;
    }

    public List<Translation> translations() {
        return translations;
    }

    public Type type() {
        return type;
    }

    public enum Type {
        Standard,
        Alternate
    }
}
