package latin.country;

public class TranslationAssembler {

    private Language language;
    private String name;
    private String transliteration;
    private boolean isPrimaryName = false;

    private TranslationAssembler() {
    }

    public static TranslationAssembler translation() {
        return new TranslationAssembler();
    }

    public TranslationAssembler withLanguage(final Language language) {
        this.language = language;
        return this;
    }

    public TranslationAssembler withName(final String name) {
        this.name = name;
        return this;
    }

    public TranslationAssembler withTransliteration(final String transliteration) {
        this.transliteration = transliteration;
        return this;
    }

    public TranslationAssembler asPrimaryName() {
        this.isPrimaryName = true;
        return this;
    }

    public Translation build() {
        return new Translation(language, name, transliteration, isPrimaryName);
    }
}