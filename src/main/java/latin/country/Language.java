package latin.country;

public enum Language {
    English,
    French,
    German,
    Spanish,
    Korean,
    Russian
}
