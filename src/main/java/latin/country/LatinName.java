package latin.country;

public final class LatinName {

    private final String name;
    private final String latinName;

    public LatinName(String name, String latinName) {
        this.name = name;
        this.latinName = latinName;
    }

    public String name() {
        return name;
    }

    public String latinName() {
        return latinName;
    }
}

