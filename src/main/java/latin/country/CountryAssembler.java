package latin.country;

import java.util.List;

public class CountryAssembler {

    private List<Name> names;

    private CountryAssembler() {
    }

    public static CountryAssembler country() {
        return new CountryAssembler();
    }


    public CountryAssembler withNames(final List<Name> names) {
        this.names = names;
        return this;
    }

    public Country build() {
        return new Country(names);
    }
}