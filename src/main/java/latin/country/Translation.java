package latin.country;

public final class Translation {
    private Language language;
    private String name;
    private String transliteration;
    private boolean isPrimaryName;

    public Translation(Language language, String name, String transliteration, boolean isPrimaryName) {
        this.language = language;
        this.name = name;
        this.transliteration = transliteration;
        this.isPrimaryName = isPrimaryName;
    }

    public Language language() {
        return language;
    }

    public String name() {
        return name;
    }

    public String transliteration() {
        return transliteration;
    }

    public boolean isPrimaryName() {
        return isPrimaryName;
    }
}
