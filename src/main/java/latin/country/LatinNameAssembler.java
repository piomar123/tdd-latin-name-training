package latin.country;

public class LatinNameAssembler {

    private String name = "";
    private String latinName = "";

    private LatinNameAssembler() {
    }

    public static LatinNameAssembler latinName() {
        return new LatinNameAssembler();
    }

    public LatinNameAssembler withName(final String name) {
        this.name = name;
        return this;
    }

    public LatinNameAssembler withLatinName(final String latinName) {
        this.latinName = latinName;
        return this;
    }

    public LatinName build() {
        return new LatinName(name, latinName);
    }
}