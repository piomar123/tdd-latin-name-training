package latin.country;

import java.util.List;

public class NameAssembler {

    private Name.Type type;
    private List<Translation> transliterations;

    private NameAssembler() {
    }

    public static NameAssembler name() {
        return new NameAssembler();
    }

    public NameAssembler withTransliterations(final List<Translation> transliterations) {
        this.transliterations = transliterations;
        return this;
    }

    public Name build() {
        return new Name(type, transliterations);
    }

    public NameAssembler asStandardType() {
        this.type = Name.Type.Standard;
        return this;
    }

    public NameAssembler asAlternateType() {
        this.type = Name.Type.Alternate;
        return this;
    }
}