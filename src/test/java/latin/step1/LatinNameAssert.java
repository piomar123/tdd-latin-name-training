package latin.step1;

import org.assertj.core.api.BDDAssertions;

import latin.country.LatinName;

public class LatinNameAssert {

    private LatinName latinName;

    private LatinNameAssert(final LatinName latinName) {
        this.latinName = latinName;
    }

    public static LatinNameAssert then(LatinName latinName) {
        return new LatinNameAssert(latinName);
    }

    public void isEqualTo(LatinName expectedLatinName) {
        BDDAssertions.then(latinName.name()).isEqualTo(expectedLatinName.name());
        BDDAssertions.then(latinName.latinName()).isEqualTo(expectedLatinName.latinName());
    }

    public LatinNameAssert hasName(String name) {
        BDDAssertions.then(latinName.name()).isEqualTo(name);
        return this;
    }
}
