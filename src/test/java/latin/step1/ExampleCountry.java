package latin.step1;

import static latin.country.CountryAssembler.country;
import static latin.country.NameAssembler.name;
import static latin.country.TranslationAssembler.translation;

import java.util.ArrayList;
import java.util.List;

import org.assertj.core.util.Lists;

import latin.country.Country;
import latin.country.Language;
import latin.country.Translation;

public class ExampleCountry {

    static Translation germanTranslation = translation()
        .withLanguage(Language.German)
        .withName("Republik Korea (Südkorea)")
        .build();
    static Translation englishTranslation = translation()
        .withLanguage(Language.English)
        .withName("Republic of Korea")
        .build();
    static Translation frenchTranslation = translation()
        .withLanguage(Language.French)
        .withName("République de Corée (Corée du Sud)")
        .build();
    static Translation koreanTranslation = translation()
        .withLanguage(Language.Korean)
        .withName("대한민국")
        .withTransliteration("Daehanmin-guk")
        .asPrimaryName()
        .build();
    static Translation spanishTranslation = translation()
        .withLanguage(Language.Spanish)
        .withName("República de Corea (Corea del Sur)")
        .build();

    private static Country getCountryForStandardTranslations(final List<Translation> standardNameTranslations) {
        var alternateNameTranslations = Lists.list(spanishTranslation);
        var standardName = name()
            .asStandardType()
            .withTransliterations(standardNameTranslations)
            .build();
        var alternateName = name()
            .asAlternateType()
            .withTransliterations(alternateNameTranslations)
            .build();
        var names = Lists.list(standardName, alternateName);
        return country()
            .withNames(names)
            .build();
    }

    public static Country givenFirstPriorityName() {
        return getCountryForStandardTranslations(Lists.list(germanTranslation, englishTranslation, frenchTranslation, koreanTranslation));
    }

    public static Country givenFrenchPriorityName() {
        return getCountryForStandardTranslations(Lists.list(germanTranslation, frenchTranslation, koreanTranslation));
    }

    public static Country givenPrimaryTransliterations() {
        return getCountryForStandardTranslations(Lists.list(germanTranslation, koreanTranslation));
    }

    public static Country givenLatinName() {
        return getCountryForStandardTranslations(Lists.list(germanTranslation));
    }
}
