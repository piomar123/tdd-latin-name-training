package latin.step1;

import static latin.country.LatinNameAssembler.latinName;
import static latin.step1.ExampleCountry.englishTranslation;
import static latin.step1.ExampleCountry.frenchTranslation;
import static latin.step1.ExampleCountry.givenFirstPriorityName;
import static latin.step1.ExampleCountry.koreanTranslation;

import org.junit.jupiter.api.Test;

import latin.country.LatinName;
import latin.retriever.LatinNameRetriever;

final class LatinNameScenarios {

    @Test
    void selectEnglishTranslation() {
        // Given
        var country = givenFirstPriorityName();

        // When
        LatinName latinName = LatinNameRetriever.getLatinName(country);

        // Then
        final LatinName expectedLatinName = latinName()
            .withName(koreanTranslation.name())
            .withLatinName(englishTranslation.name())
            .build();
        LatinNameAssert.then(latinName).isEqualTo(expectedLatinName);
    }

    @Test
    void selectFrenchTranslation() {
        // Given
        var country = ExampleCountry.givenFrenchPriorityName();

        // When
        LatinName latinName = LatinNameRetriever.getLatinName(country);

        // Then
        final LatinName expectedLatinName = latinName()
            .withName(koreanTranslation.name())
            .withLatinName(frenchTranslation.name())
            .build();
        LatinNameAssert.then(latinName).isEqualTo(expectedLatinName);
    }

    @Test
    void selectPrimaryNameTransliteration() {
        // Given
        var country = ExampleCountry.givenPrimaryTransliterations();

        // When
        LatinName latinName = LatinNameRetriever.getLatinName(country);

        // Then
        final LatinName expectedLatinName = latinName()
            .withName(koreanTranslation.name())
            .withLatinName(koreanTranslation.transliteration())
            .build();
        LatinNameAssert.then(latinName).isEqualTo(expectedLatinName);
    }

    @Test
    void selectEmptyLatinName() {
        // Given
        var country = ExampleCountry.givenLatinName();

        // When
        LatinName latinName = LatinNameRetriever.getLatinName(country);

        // Then
        final LatinName expectedLatinName = latinName().build();
        LatinNameAssert.then(latinName).isEqualTo(expectedLatinName);
    }
}
